package com.t1.yd.tm.api.model;

import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    void setStatus(@NotNull Status status);

    @NotNull
    Status getStatus();

}
