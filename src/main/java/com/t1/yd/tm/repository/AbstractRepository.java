package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator comparator) {
        @Nullable final List<E> result = new ArrayList<>(entities);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public E findOneById(final @NotNull String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(final @NotNull Integer index) {
        return entities.get(index);
    }

    @Nullable
    @Override
    public E removeById(final @NotNull String id) {
        @Nullable final E entity = findOneById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        return findOneById(id) != null;
    }

    public int getSize() {
        return entities.size();
    }

}
