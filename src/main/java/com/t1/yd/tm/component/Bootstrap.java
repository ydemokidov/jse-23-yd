package com.t1.yd.tm.component;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.command.project.*;
import com.t1.yd.tm.command.system.*;
import com.t1.yd.tm.command.task.*;
import com.t1.yd.tm.command.user.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.system.ArgumentNotSupportedException;
import com.t1.yd.tm.exception.system.CommandNotSupportedException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRegistryCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
    }

    public void run(String[] args) {
        initDemoData();
        initLogger();

        processArguments(args);

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void initDemoData() {
        userService.add(userService.create("admin", "admin", "admin@mail.ru", Role.ADMIN));
        userService.add(userService.create("user1", "user1", "user1@mail.ru"));
        userService.add(userService.create("user2", "user2", "user2@mail.ru"));

        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project1", "my first project"));
        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project2", "my 2nd project"));
        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project3", "my 3rd project"));
        projectRepository.add(userService.findByLogin("user2").getId(), new Project("project4", "my 4th project"));
        projectRepository.add(userService.findByLogin("user2").getId(), new Project("project5", "my 5th project"));

        taskRepository.add(userService.findByLogin("user1").getId(), new Task("task1", "my 1st task"));
        taskRepository.add(userService.findByLogin("user1").getId(), new Task("task2", "my 2nd task"));
        taskRepository.add(userService.findByLogin("user2").getId(), new Task("task3", "my 3rd task"));
        taskRepository.add(userService.findByLogin("user2").getId(), new Task("task4", "my 4th task"));

        projectTaskService.bindTaskToProject(userService.findByLogin("user1").getId(), taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(userService.findByLogin("user1").getId(), taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException();
        command.execute();
    }

    private void processCommand(@NotNull final String commandName) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private void exit() {
        System.exit(0);
    }

}