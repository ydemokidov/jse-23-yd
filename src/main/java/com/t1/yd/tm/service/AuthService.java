package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.system.PermissionException;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.exception.user.IncorrectLoginOrPasswordException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new IncorrectLoginOrPasswordException();
        @NotNull final String pwdHash = HashUtil.salt(password);
        if (!pwdHash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(getUserId());
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
